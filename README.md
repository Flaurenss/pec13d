# Introduction

Ghost Race is a prototype where the player has to complete three laps trying to achieve the best times against the ghost of the best lap.

## Game instructions

Use Arrow Keys / WASD for car movement.

SPACE bar to activate the handbrake.

ESC key allows the user to quit game while the player is at main menu or restart the game while playing.

Compelte three laps while competeing with the ghost of your best lap to end the race.

After finishing three laps you will be able to see your replay by pressing on "Replay" button. 

## Dev Information

This was an originary project from the UOC Game Development Posgrade. Now it has been redone in order to provide a better experienice and also to improve my skills codign a better replay system (with the include of particle system and sounds).

You can play and see more information about the game at: https://braverobot.itch.io/ghost-race




