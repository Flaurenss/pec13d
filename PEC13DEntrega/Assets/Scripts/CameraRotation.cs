﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityStandardAssets.Vehicles.Car;

public class CameraRotation : MonoBehaviour
{
    public GameObject Car;
    public CinemachineVirtualCamera BackCamera;
    public CinemachineVirtualCamera FrontCamera;
    //The max velocity that the car can go backwards
    public float MaxNVelocity;
    public float MaxPVelocity;
    public PlayableDirector Timeline;

    private CarController carController;
    private bool isDefaultCamera;
    // Start is called before the first frame update
    private void Start()
    {
        carController = Car.GetComponent<CarController>();
        isDefaultCamera = true;
    }

    private void FixedUpdate()
    {
        // var miau = Vector3.Dot(Car.transform.forward, (carController.RigidBodyVelocity));
        //Debug.Log(miau);
        ControlCameraRotation();
    }

    /// <summary>
    /// Controls when the car have negative velocity in order to rotate the camera to the front
    /// </summary>
    private void ControlCameraRotation()
    {
        var direction = Vector3.Dot(Car.transform.forward, carController.RigidBodyVelocity);
        if (direction < MaxNVelocity)
        {
            //Camera transition to forwards
            BackCamera.gameObject.SetActive(false);

        }
        else if(direction > MaxPVelocity)
        {
            //Camra transition to backwards
            BackCamera.gameObject.SetActive(true);

        }
    }
}
