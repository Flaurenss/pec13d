﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using UnityStandardAssets.Vehicles.Car;

public class GameManager : MonoBehaviour
{
    public AudioSource countDownAudio;
    public GameObject car;
    public GameObject cameraRotationController;
    public TextMeshProUGUI CountDownText;
    public TextMeshProUGUI LapTitle;
    public TextMeshProUGUI LapNumber;
    public GameObject TimeContainer;
    public TextMeshProUGUI ActualTimeCounter;
    public TextMeshProUGUI TimeCounterLap1;
    public TextMeshProUGUI TimeCounterLap2;
    public static bool StartGameBtnPressed = false;

    private float CurrCountDownValue;
    private float CurrLapNumber;
    private CarRecorder carRecorderController;
    private LapTimeManager lapTimeController;
    private Animator countdownAnimator;
    private CarController carController;
    private Rigidbody carRigidBody;
    private CarUserControl userControl;
    
    // Start is called before the first frame update
    void Start()
    {
        userControl = car.GetComponent<CarUserControl>();
        userControl.enabled = false;
        carRigidBody = car.GetComponent<Rigidbody>();
        carController = carRigidBody.gameObject.GetComponent<CarController>();
        countdownAnimator = CountDownText.gameObject.GetComponent<Animator>();
        CountDownText.gameObject.SetActive(false);
        CurrCountDownValue = 3f;
        CountDownText.text = CurrCountDownValue.ToString();
        LapTitle.enabled = false;
        LapNumber.enabled = false;
        CurrLapNumber = 0;
        // carRigidBody.drag = 50f;
        carRecorderController = GetComponent<CarRecorder>();
        lapTimeController = GetComponent<LapTimeManager>();
    }

    private async Task Update()
    {
        if(StartGameBtnPressed)
        {
            userControl.enabled = true;
            carRigidBody.drag = 50f;
            StartGameBtnPressed = false;
            await CountdownAsync();
            // Debug.Log("Count down finished");
        }
    }
    async Task CountdownAsync()
    {
        car.SetActive(true);
        cameraRotationController.SetActive(true);
        CountDownText.gameObject.SetActive(true);
        countDownAudio.Play();
        while(CurrCountDownValue > 0)
        {
            countdownAnimator.SetTrigger("CountAction");
            await Task.Delay(1000);
            countdownAnimator.ResetTrigger("CountAction");
            CurrCountDownValue--;
            CountDownText.text = CurrCountDownValue.ToString();
        }
        CountDownText.text = "GO!";
        carController.countdownEnd = true;
        carRigidBody.drag = 0.1f;
        countdownAnimator.SetTrigger("CountAction");
        CarRecorder.StartRecording = true;
        CarRecorder.StartRecordingReplay = true;
        
        LapTitle.enabled = true;
        LapNumber.enabled = true;
        TimeContainer.SetActive(true);
        LapTimeManager.StartLapCounter = true;
        await Task.Delay(1500);
        CountDownText.gameObject.SetActive(false);
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            if (CurrLapNumber < 4)
            {
                //New lap
                CurrLapNumber++;
                if(CurrLapNumber != 4)
                {
                    LapNumber.text = (CurrLapNumber + "/3").ToString();
                }
                CheckLapNumber(CurrLapNumber);
                if(!CarRecorder.PlayRecord && CurrLapNumber < 4 && CurrLapNumber!= 1)
                {
                    carRecorderController.StartFakeCar();
                }
            }
            if(CurrLapNumber > 3)
            {
                //End race
                TimeContainer.gameObject.SetActive(false);
                LapTitle.gameObject.SetActive(false);
                LapNumber.gameObject.SetActive(false);
                carRigidBody.gameObject.SetActive(false);
                CarRecorder.StartRecording = false;
                CarRecorder.StartRecordingReplay = false;
                LapTimeManager.StartLapCounter = false;
                carRecorderController.ResetFakeCar();
                MenuManager.EndRace = true;
            }
        }
    }

    /// <summary>
    /// Check in wat lap is the car in roder to reestructurate the timers info
    /// </summary>
    private void CheckLapNumber(float lapNumber)
    {

        if(lapNumber == 2)
        {
            TimeCounterLap1.text = ActualTimeCounter.text;
            TimeCounterLap1.gameObject.SetActive(true);
        }
        else if(lapNumber == 3)
        {
            TimeCounterLap2.text = ActualTimeCounter.text;
            TimeCounterLap2.gameObject.SetActive(true);
        }

        if (lapNumber != 1 && lapNumber != 4)
        {
            lapTimeController.ResetCounter();
        }
    }
}
