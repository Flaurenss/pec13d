﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class MenuManager : MonoBehaviour
{
    public GameObject MenuCamera;
    public GameObject StartButton;
    public GameObject ReplayButton;
    public static bool EndRace = false;

    [SerializeField]
    private GameObject exitTextUI;
    private bool ReplayButtonAppeared = false;
    private CarRecorder carRecorderController;
    [SerializeField]
    private GameObject topFlag;
    [SerializeField]
    private GameObject bottomFlag;
    private Animator _topFlagAnim;
    private Animator _bottomFlagAnim;
    private bool startButtonPressed = false;

    private void Start()
    {
        _topFlagAnim = topFlag.GetComponent<Animator>();
        _bottomFlagAnim = bottomFlag.GetComponent<Animator>();
        carRecorderController = GetComponent<CarRecorder>();
    }

    public void StartGame()
    {
        Debug.Log("Start pressed");
        startButtonPressed = true;
        exitTextUI.SetActive(true);
        _topFlagAnim.SetTrigger("start");
        _bottomFlagAnim.SetTrigger("start");
        StartButton.SetActive(false);
        MenuCamera.gameObject.SetActive(false);
        GameManager.StartGameBtnPressed = true;
    }

    public void ReplayRace()
    {
        ReplayButton.SetActive(false);
        CarRecorder.PlayPlayerRecord = true;
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.Escape) && !startButtonPressed)
        {
            Application.Quit();
        }
        else if (Input.GetKey(KeyCode.Escape))
        {
            carRecorderController.ResetFakeCar();
            CarRecorder.PlayPlayerRecord = false;
            EndRace = false;
            SceneManager.LoadScene("RaceTerrainScene");
        }

        if(EndRace && !ReplayButtonAppeared)
        {
            ReplayButtonAppeared = true;
            ReplayButton.gameObject.SetActive(true);
        }
    }
}
