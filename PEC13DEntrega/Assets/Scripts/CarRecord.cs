﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class CarRecord
{
    public List<Vector3> CarPosition;
    public List<Quaternion> CarRotation;
    public List<Quaternion> WheelRRotation;
    public List<Quaternion> WheelLRotation;
    public List<float> Revs;
    public List<float> BrakeInput;
    //Store info of every wheel collider in order to know what particle system/sound needs to be activated
    //public List<Dictionary<int, float>> WheelsData;
    public List<WheelsData> WheelsInfo;
}

public class WheelsData
{
    public int index;
    public float DataForward;
    public float DataSideways;

    public WheelsData(int i, float dF, float dS)
    {
        this.index = i;
        this.DataForward = dF;
        this.DataSideways = dS;
    }
}
