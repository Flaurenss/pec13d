﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine.Playables;

public class CarRecorder : MonoBehaviour
{
    public PlayableDirector ReplayTimeline;
    public GameObject Car;
    public GameObject FakeCar;
    public GameObject ReplayCar;
    public GameObject WheelR;
    public GameObject WheelL;
    public GameObject ReplayWheelR;
    public GameObject ReplayWheelL;
    public GameObject ReplayCamera;
    public GameObject[] ReplayCameras;
    [HideInInspector] public static CarRecord CarRecord;
    [HideInInspector] public static CarRecord CarRecordSaved;
    [HideInInspector] public static CarRecord CarRecordReplay;
    [HideInInspector] public static CarRecord CameraRecordReplay;
    public static bool StartRecording = false;
    public static bool StartRecordingReplay = false;
    public static bool PlayRecord = false;
    public static bool PlayPlayerRecord = false;

    private static float Timer = 0;
    private static float TimerRecordRace = 0;
    private float CameraTimer = 0;
    private float FakeTimer = 0;
    private float TimerPlayerRecord = 0;
    private float TimeFreq = 0.2f;
    private static int index = 0;
    private static int indexReplay = 0;
    private float elapsedTime = 0;
    private CarController carController;
    private CarController replayCarController;
    Vector3 posA;
    Vector3 posB;
    Quaternion rotA;
    Quaternion rotB;
    Quaternion wheelRrotA;
    Quaternion wheelRrotB;
    Quaternion wheelLrotA;
    Quaternion wheelLrotB;
    bool lastWasActive = false;

    float RevA;
    float RevB;

    float BrakeA;
    float BrakeB;

    int wheelIndexA;
    int wheelIndexB;
    float ForwardA;
    float ForwardB;
    float SidewaysA;
    float SidewaysB;

    private void Start()
    {
        CarRecord = InitRecord(CarRecord);
        CarRecordSaved = InitRecord(CarRecordSaved);
        CarRecordReplay = InitRecord(CarRecordReplay);
        carController = Car.GetComponent<CarController>();
        replayCarController = ReplayCar.GetComponent<CarController>();
        PlayRecord = false;
        lastWasActive = false;
        FakeCar.SetActive(false);
    }

    private static CarRecord InitRecord(CarRecord record)
    {
        record = new CarRecord()
        {
            CarPosition = new List<Vector3>(),
            CarRotation = new List<Quaternion>(),
            WheelLRotation = new List<Quaternion>(),
            WheelRRotation = new List<Quaternion>(),
            Revs = new List<float>(),
            BrakeInput = new List<float>(),
            WheelsInfo = new List<WheelsData>()
        };
        return record;
    }

    private void FixedUpdate()
    {
        //Debug.Log(WheelR.transform.localEulerAngles.y);

        if (StartRecording)
        {
            RecordRace(CarRecord, ref Timer, false);
        }
        if (StartRecordingReplay)
        {
            RecordRace(CarRecordReplay, ref TimerRecordRace, true);
        }

        if (PlayRecord)
        {
            if (!lastWasActive)
            {
                SetFakeCarInitialPos(FakeCar, CarRecordSaved, false);
            }
            PlaySavedRecord(CarRecordSaved);
        }

        if (PlayPlayerRecord)
        {
            ReplayTimeline.Play();
            if (!lastWasActive)
            {
                SetFakeCarInitialPos(ReplayCar, CarRecordReplay, true);
                // ReplayCamera.SetActive(true);
                EnableReplayCameras();
            }
            PlayReplayRecord();
        }
        else
        {
            ReplayCar.SetActive(false);
            ReplayTimeline.Stop();
            //ReplayCamera.SetActive(false);
            DisabelReplayCameras();
        }
    }

    void EnableReplayCameras()
    {
        foreach (var camera in ReplayCameras)
        {
            camera.SetActive(true);
        }
    }

    void DisabelReplayCameras()
    {
        foreach (var camera in ReplayCameras)
        {
            camera.SetActive(false);
        }
    }
    private void SetFakeCarInitialPos(GameObject fakeCar, CarRecord recordSaved, bool replayMode)
    {
        var fakeController = fakeCar.GetComponent<CarController>();
        lastWasActive = true;
        elapsedTime = 0;
        fakeCar.transform.position = posA = recordSaved.CarPosition[0];
        fakeCar.transform.rotation = rotA = recordSaved.CarRotation[0];
        if (replayMode)
        {
            ReplayWheelR.transform.localRotation = wheelRrotA = recordSaved.WheelRRotation[0];
            ReplayWheelL.transform.localRotation = wheelLrotA = recordSaved.WheelLRotation[0];

            wheelRrotB = recordSaved.WheelRRotation[1];
            wheelLrotB = recordSaved.WheelLRotation[1];

            //TODO acces user saved inputs
            fakeController.Revs = RevA = recordSaved.Revs[0];
            RevB = recordSaved.Revs[1];

            fakeController.BrakeInput = BrakeA = recordSaved.BrakeInput[0];
            BrakeB = recordSaved.BrakeInput[1];

            fakeController.ForwardSlip = ForwardA = recordSaved.WheelsInfo[0].DataForward;
            ForwardB = recordSaved.WheelsInfo[1].DataForward;
            fakeController.SidewaysSlip = SidewaysA = recordSaved.WheelsInfo[0].DataSideways;
            SidewaysB = recordSaved.WheelsInfo[1].DataSideways;
            fakeController.WheelIndex = wheelIndexA = recordSaved.WheelsInfo[0].index;
            wheelIndexB = recordSaved.WheelsInfo[1].index;
        }
        posB = recordSaved.CarPosition[1];
        rotB = recordSaved.CarRotation[1];
        index = 2;
        fakeCar.gameObject.SetActive(true);
    }

    private void RecordRace(CarRecord saveCar, ref float timer, bool replayMode)
    {
        timer += Time.fixedDeltaTime;
        if (timer >= TimeFreq)
        {
            saveCar.CarPosition.Add(Car.transform.position);
            saveCar.CarRotation.Add(Car.transform.rotation);
            if (replayMode)
            {
                saveCar.WheelRRotation.Add(WheelR.transform.localRotation);
                saveCar.WheelLRotation.Add(WheelL.transform.localRotation);
                saveCar.Revs.Add(carController.Revs);
                saveCar.BrakeInput.Add(carController.BrakeInput);
                saveCar.WheelsInfo.Add(new WheelsData(carController.WheelIndex, carController.ForwardSlip, carController.SidewaysSlip));
            }
            timer = 0;
        }
    }

    private void PlaySavedRecord(CarRecord savedRecord)
    {
        elapsedTime += Time.fixedDeltaTime;
        if (elapsedTime >= TimeFreq)
        {
            elapsedTime -= TimeFreq;
            NextPositions(savedRecord, false);
        }
        var timeBtwSamples = elapsedTime / TimeFreq;
        FakeCar.transform.position = Vector3.Lerp(posA, posB, timeBtwSamples);
        FakeCar.transform.rotation = Quaternion.Lerp(rotA, rotB, timeBtwSamples);
    }

    private void NextPositions(CarRecord savedRecord, bool replayMode)
    {
        index++;
        if (index == savedRecord.CarPosition.Count)
        {
            FakeCar.gameObject.SetActive(false);
            //TODO check if its replay system or ghost car
            if (PlayRecord)
            {
                PlayRecord = false;
            }
            else
            {
                PlayPlayerRecord = false;
            }
            index = 0;
        }
        else
        {
            posA = posB;
            posB = savedRecord.CarPosition[index];

            rotA = rotB;
            rotB = savedRecord.CarRotation[index];
            if (replayMode)
            {
                wheelRrotA = wheelRrotB;
                wheelRrotB = savedRecord.WheelRRotation[index];

                wheelLrotA = wheelLrotB;
                wheelLrotB = savedRecord.WheelLRotation[index];

                //TODO access new input value
                RevA = RevB;
                RevB = savedRecord.Revs[index];

                BrakeA = BrakeB;
                BrakeB = savedRecord.BrakeInput[index];

                SidewaysA = SidewaysB;
                SidewaysB = savedRecord.WheelsInfo[index].DataSideways;
                ForwardA = ForwardB;
                ForwardB = savedRecord.WheelsInfo[index].DataForward;
                wheelIndexA = wheelIndexB;
                wheelIndexB = savedRecord.WheelsInfo[index].index;
            }
        }
    }

    private void PlayReplayRecord()
    {
        ReplayCar.gameObject.SetActive(true);
        TimerPlayerRecord += Time.fixedDeltaTime;
        if (TimerPlayerRecord >= TimeFreq)
        {
            TimerPlayerRecord -= TimeFreq;
            NextPositions(CarRecordReplay, true);
        }
        var timeBtwSamples = TimerPlayerRecord / TimeFreq;
        ReplayCar.transform.position = Vector3.Lerp(posA, posB, timeBtwSamples);
        ReplayCar.transform.rotation = Quaternion.Lerp(rotA, rotB, timeBtwSamples);
        ReplayWheelR.transform.localRotation = Quaternion.Lerp(wheelRrotA, wheelRrotB, timeBtwSamples);
        ReplayWheelL.transform.localRotation = Quaternion.Lerp(wheelLrotA, wheelLrotB, timeBtwSamples);
        replayCarController.Revs = Mathf.Lerp(RevA, RevB, timeBtwSamples);
        replayCarController.BrakeInput = Mathf.Lerp(BrakeA, BrakeB, timeBtwSamples);
        //Debug.Log(string.Format("indexA = {0} indexB = {1}", wheelIndexA, wheelIndexB));
        replayCarController.CheckWheelSpinFake(Mathf.Lerp(ForwardA, ForwardB, timeBtwSamples), Mathf.Lerp(SidewaysA, SidewaysB, timeBtwSamples), (int)Mathf.Lerp(wheelIndexA, wheelIndexB, timeBtwSamples));
    }

    public void SaveRecord()
    {
        CarRecordSaved = InitRecord(CarRecordSaved);
        CarRecordSaved = CarRecord;
        CarRecord = InitRecord(CarRecord);
    }

    public void ResetFakeCar()
    {
        PlayRecord = false;
        FakeCar.SetActive(false);
        lastWasActive = false;
    }

    public void StartFakeCar()
    {
        index = 0;
        PlayRecord = true;
    }
}
