﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapTime
{
    public float TotalTime;
    public int MilliSeconds;
    public int Seconds;
    public int Minutes;
}
