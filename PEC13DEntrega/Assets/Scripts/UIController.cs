﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    private Animator uiAnim;
    // Start is called before the first frame update
    void Start()
    {
        uiAnim = GetComponent<Animator>();
    }

    public void OnPointerEnter()
    {
        uiAnim.SetBool("mouseOver", true);
    }

    public void OnPointerExit()
    {
        uiAnim.SetBool("mouseOver", false);
    }
}
