﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LapTimeManager : MonoBehaviour
{
    public static int MilliCount;
    public static int SecondsCount;
    public static int MinutesCount;

    public TextMeshProUGUI TimeBox;

    public static bool StartLapCounter;
    [HideInInspector]public static LapTime LastTime;
    private float totalTime;

    private CarRecorder carRecorderManager;

    private void Start()
    {
        LastTime = new LapTime();
        StartLapCounter = false;
        carRecorderManager = GetComponent<CarRecorder>();
    }

    // Update is called once per frame
    void Update()
    {
        if(StartLapCounter)
        {
            totalTime += Time.deltaTime;
            MinutesCount = (int)(totalTime / 60f);
            SecondsCount = (int)totalTime - 60 * MinutesCount;
            MilliCount = (int)(1000 * (totalTime - MinutesCount * 60 - SecondsCount));
            UpdateTimeBox(MinutesCount, SecondsCount, MilliCount);
        }
    }

    private void UpdateTimeBox(int minutes, int seconds, float miliseconds)
    {
        TimeBox.text = minutes.ToString("D2") + "'" + seconds.ToString("D2") + "''" + miliseconds.ToString("F0");
    }

    public void ResetCounter()
    {
        SaveLastTime();
        MilliCount = 0;
        SecondsCount = 0;
        MinutesCount = 0;
        totalTime = 0;
    }

    public void SaveLastTime()
    {
        if (LastTime.MilliSeconds == 0 && LastTime.Seconds == 0 && LastTime.Minutes == 0)
        {
            LastTime.TotalTime = totalTime;
            LastTime.MilliSeconds = MilliCount;
            LastTime.Seconds = SecondsCount;
            LastTime.Minutes = MinutesCount;
            carRecorderManager.SaveRecord();
            carRecorderManager.StartFakeCar();
        }
        //MinutesCount <= LastTime.Minutes && SecondsCount <= LastTime.Seconds
        else if (totalTime <= LastTime.TotalTime)
        {
            LastTime.TotalTime = totalTime;
            LastTime.MilliSeconds = MilliCount;
            LastTime.Seconds = SecondsCount;
            LastTime.Minutes = MinutesCount;
            carRecorderManager.SaveRecord();
            carRecorderManager.ResetFakeCar();
            carRecorderManager.StartFakeCar();
            Debug.Log("Mejor vuelta");
        }
        else
        {
            //in case that the player arrives late than the ghost car
            Debug.Log("you're late");
            carRecorderManager.ResetFakeCar();
            carRecorderManager.StartFakeCar();
        }
    }
}
